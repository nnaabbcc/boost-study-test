
add_executable(boost_file_system_study
    file_system_study.cpp
)
target_link_libraries(boost_file_system_study PRIVATE Boost::filesystem)
