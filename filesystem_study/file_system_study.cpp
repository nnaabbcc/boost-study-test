
#include <iostream>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

int main(int, char**)
{
    fs::path p("c:\\");
    std::cout << p << " " << p.is_absolute() << std::endl;
    for (fs::directory_iterator item(p); item != fs::directory_iterator(); item++)
    {
        std::cout << *item << std::endl;
    }
    return 0;
}