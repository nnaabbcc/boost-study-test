#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <iostream>

using namespace boost::gregorian;

int main(int, char**)
{
    auto t = boost::posix_time::second_clock::local_time();
    std::cout << t.date() << std::endl;
    std::cout << t.time_of_day() << std::endl;
    std::cout << t << std::endl;

    return 0;
}