#include <boost/date_time/gregorian/gregorian.hpp>
#include <iostream>

using namespace boost::gregorian;

int main(int, char**)
{
    auto today = day_clock::local_day();
    std::cout << today << std::endl;

    auto today_string = to_iso_string(today);
    std::cout << today_string << std::endl;

    return 0;
}
