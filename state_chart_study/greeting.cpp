#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/simple_state.hpp>
#include <iostream>

namespace sc = boost::statechart;

struct Greeting;

struct Machine : sc::state_machine<Machine, Greeting>
{};

struct Greeting : sc::simple_state<Greeting, Machine>
{
    Greeting()
    {
        std::cout << "Hello World!" << std::endl;
    }

    ~Greeting()
    {
        std::cout << "Bye bye World!" << std::endl;
    }

};

int main(int, char**)
{
    Machine myMachine;

    myMachine.initiate();

    return 0;
}
